# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field

class AllvrnItem(Item):
    rubric 	= Field()
    name 	= Field()
    addr	= Field()
    tel		= Field()
    site	= Field()
    email	= Field()
    about	= Field()
    url		= Field()