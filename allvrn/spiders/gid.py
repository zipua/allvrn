# -*- coding: utf-8 -*-

from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.http import Request
from scrapy.utils.markup import remove_tags

from allvrn.items import AllvrnItem

import re

#import logging
#from scrapy.log import ScrapyFileLogObserver

#logfile = open('testlog.log', 'w')
#log_observer = ScrapyFileLogObserver(logfile, level=logging.DEBUG)
#log_observer.start()

#import scrapy.log

#scrapy.log.start(logfile='error', loglevel = scrapy.log.ERROR)
#scrapy.log.start(logfile='debug', loglevel = scrapy.log.DEBUG)


class GidSpider(CrawlSpider):
    name = 'gid'
    start_urls = ['http://gid.allvrn.ru/']

    rules = (
        Rule(SgmlLinkExtractor(allow=r'/rubric/section/id/([0-9]+)$'), callback='parse_page', follow=True),
        Rule(SgmlLinkExtractor(allow=r'/rubric/section/id/([0-9]+)/\?page=([0-9]+)$'), callback='parse_page', follow=True),
    )

    def parse_page(self, response):
        hxs = HtmlXPathSelector(response)
        
        rubric = hxs.select('//div/h1/text()')[0].extract()
        rubric = rubric.replace(u' в Воронеже', '')

        firms = hxs.select('//td[@class="center"]')
        for firm in firms:
            url = firm.select('./a/@href')[0].extract()
            if 'firm/id' in url:
                callback = self.parse_item
            else:
                callback = self.parse_item_contacts
                url = '%s/contacts/' % url
            yield Request(url, callback = callback, meta = {'rubric':rubric})


    def parse_item(self, response):
        hxs = HtmlXPathSelector(response)
        i = AllvrnItem()

        rubric = response.meta['rubric']
        url  = response.url
        name = hxs.select('//div[@class="titleFil"]/h1/text()')[0].extract()
        
        email = ''
        site  = ''
        cont = hxs.select('//div[@class="cont"]')
        links = cont.select('.//a')
        for link in links:
            test_link = link.select('@href')[0].extract()
            if 'mailto:' in test_link:
                email = test_link.replace('mailto:', '')
            else:
                site = test_link

        addr = ''
        tel  = ''
        cont_text = cont[0].extract()
        cont_lines = cont_text.split('<br>')
        for cont_line in cont_lines:
            if 'cont1.gif' in cont_line:
                addr = cont_line.replace('<img src="images/yp/cont1.gif" alt=""> ','')
            elif 'cont2.gif' in cont_line:
                tel = cont_line.replace('<img src="images/yp/cont2.gif" alt=""> ', '')

        i['rubric'] = rubric
        i['name']   = name
        i['addr']   = addr
        i['tel']    = tel
        i['site']   = site
        i['email']  = email
        i['about']  = ''
        i['url']    = url

        return i


    def parse_item_contacts(self, response):
        hxs = HtmlXPathSelector(response)
        i = AllvrnItem()

        rubric  = response.meta['rubric']
        name    = hxs.select('//noindex/div[@class="title"]/a/text()')[0].extract()
        addr    = hxs.select('//div[@class="info address"]/text()')[0].extract()
        tel     = hxs.select('//div[@class="info phones"]/text()')[0].extract()
        try:
            site    = hxs.select('//div[@class="info site"]/a/@href')[0].extract()
        except:
            site = ''
        email   = ''
        url     = response.url.replace('/contacts/', '')
        
        try:
            emailjs = hxs.select('//div[@class="info email"]')[0].extract()
        except:
            emailjs = ''
        email_digits = re.findall('(?<=L=c\()([0-9,]+)', emailjs)
        if email_digits:
            digits = email_digits[0]
            digits = digits.split(',')
            digits = [chr(int(d)) for d in digits]
            email  = ''.join(digits)

        requrl = '%s/about' % url
        
        i['rubric'] = rubric
        i['name']   = name
        i['addr']   = addr
        i['tel']    = tel
        i['site']   = site
        i['email']  = email
        i['url']    = url
        
        yield Request(requrl, callback = self.parse_item_about, meta = {'item':i})


    def parse_item_about(self, response):
        hxs = HtmlXPathSelector(response)
        i = response.meta['item']

        about = hxs.select('//div[@style="clear: both;"]')[0].extract()
        about = remove_tags(about)
        i['about'] = about

        return i
