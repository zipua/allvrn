# Scrapy settings for allvrn project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'allvrn'

SPIDER_MODULES = ['allvrn.spiders']
NEWSPIDER_MODULE = 'allvrn.spiders'

#DEPTH_PRIORITY = 1
#SCHEDULER_DISK_QUEUE = 'scrapy.squeue.PickleFifoDiskQueue'
#SCHEDULER_MEMORY_QUEUE = 'scrapy.squeue.FifoMemoryQueue'

FEED_EXPORTERS = {
    'csv': 'allvrn.feedexport.CSVkwItemExporter'
}

EXPORT_FIELDS = [
    'rubric',
    'name',
    'addr',
    'tel',
    'site',
    'email',
    'about',
    'url'
]

#LOG_FILE  = 'log.txt'
#LOG_LEVEL = 'ERROR'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'allvrn (+http://www.yourdomain.com)'
